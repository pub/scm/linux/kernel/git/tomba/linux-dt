# Upstream DT Overlays and TI kernel config fragments

This is a repository for DT overlays and TI kernel config fragments.

## Branches

`master` branch is the development branch and works with Linus' upstream `master` branch.

When Linus tags a new kernel version, a branch will be created for that kernel version, e.g. `linux-5.7.y`. If stable releases for that kernel version will cause breakage with DT or configs, which is unlikely but possible, the patches can be added to `linux-5.7.y` to make it work with latest stable of that version.

`ti-linux-x.y.z` branch is the development branch that works with the TI productized `ti-linux-x.y.z` kernel branch. When the kernel ti-linux branch is tagged this repo and branch will be tagged with the corresponding tag.

Patches to the overlays should always be submitted to the `master` branch and if applicable back ported to the `ti-linux-x.y.z` branch. Only patches that are applicable to the `ti-linux-x.y.z` features should be submitted to that branch.

## DT Overlays

The makefile does two things:

- Compiles a pre-defined set of dts files with symbols enabled, taken from the kernel source tree
- Compiles DT overlays in this repository

Linux directory can be given with `LINUX=dir`. If LINUX is undefined, Makefile will look for Linux directory from the current dir or `<projectdir>/../linux/`

dtc compiler will be taken from `$LINUX/scripts/dtc/dtc` but can be overridden with DTC variable. Note that if you use dtc from the kernel directory, you need to have built it first (e.g. `make dtbs` in kernel).

Output path can be given the same way as when building the kernel, with `O=dir`

ARCH variable can be used to build a single arch instead of all, e.g. `ARCH=arm64`

## Testing DT Overlays

The Makefile has `test` target which applies overlays to base dtb using `fdtapply` and reports if the apply succeeded. The Makefile has a list of base-dts + overlay-dtbos to define how to apply the overlays.

This provides an easy way to test if overlays apply correctly. When adding new overlays, also add new test targets to the Makefile.

## Config fragments

config directory contains defconfig_builder.sh. The Linux directory can be given with -k option. If Linux directory is not given, the script will look for Linux directory from the current dir or `<projectdir>/../linux/`

## Example developer workflow

Linux sources in ~/work/linux and this project in ~/work/linux-dt

```
$ cd ~/work/linux
$ ../linux-dt/config/defconfig_builder.sh
(select a defconfig)
$ make ti_sdk_dra7x_debug_defconfig
$ make
$ make -C ../linux-dt

The symbol-enabled dtbs and dtbos are now under ~/work/linux-dt/arch/...
```
