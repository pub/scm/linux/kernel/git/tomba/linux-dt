# System Test Build Script

This directory contains the scripts to build the system test builds.

## Directory Contents

This directory contains a build script and recipe files to create different test
builds for test automation.

The build_system_test.sh script takes a recipe file and creates builds based on
the recipe.  The recipe can contain defconfig builds, patch builds and dt overlay
build instructions.  The format of the recipe file is below.

The defconfig_builds can either be in tree or TI specific defconfigs.  The TI
specific defconfig builds are created by the defconfig_builder script located
in this repo under the configs directory.  This script also has a recipe file on
how to build a specific defconfig for the target.

The dt overlay builds are created through the Makefile of the arch directory
within this repo.  All dt overlays for a given architecture within this repo
will be built and the dt_overlay instruction indicates what directory to copy
the dtbo files from.

The base_build_recipe contains a list of both in tree and TI specific defconfig
builds.  

## Build Script arguments

CROSS_COMPILE - is taken as an enviroment variabe

ARCH - can be taken as an enviroment variable or passed into the script

Required arguements:

	-k - Kernel Source directory

	-m - The map with the test suite name and the corresponding information

	-d - The defconfig to be built from the defconfig builder.

	-b - output directory for the build

	-t - TestSuite to be built.  This will also be appeneded to the output
	     build directory

Optional arguements:

	-a - Arch to build either "arm" or "arm64" this can also be exported as an enviroment variable

	-o - DT overlay target directory

	-w - Working directory for the test scripts.

Example:
	./build_system_test.sh -k ~/linux_kernel/upstream/ -b ~/Desktop/build_out -t j7_usb_otg_high_speed -d ti_sdk_arm64_release -a arm64 -o usb_otg/j7/high_speed

## Recipe File

The recipe file gives the build_system_test.sh the instructions on how to build
each system test target.

arch: - This is the architecture to build this must be either arm or arm64

testsuite: - Is the tests that is to be executed.  This will also be the output
directory name on the build out.

defconfig: - This is the defconfig to build. This can either be an in tree defconfig
or a TI specific defconfig.

patch_dir: - Directory that contains patches that need to be applied

overlay_dir: - Is the directory that the dtbo files for a specific system test
will be.

In tree defconfig example:

	arch: arm testsuite: multi_v7_defconfig defconfig: multi_v7_defconfig patch_dir: none overlay_dir: none

System Test Defconfig build example:

	arch: arm testsuite: am4_mmc defconfig: am4_mmc_module patch_dir: none overlay_dir: none

DT Overlay example:

	arch: arm testsuite: k2e_nand defconfig: ti_sdk_keystone_release patch_dir: none dt_overlay: nand_dma/k2
