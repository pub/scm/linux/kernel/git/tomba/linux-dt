#!/bin/bash -x
#  Copyright (C) 2020 Texas Instruments Incorporated - http://www.ti.com/ 
#  ALL RIGHTS RESERVED

ARM64_DT_PATH="arch/arm64/boot/dts/ti"
ARM_DT_PATH="arch/arm/boot/dts"

# Template for temporary build files
TMP_PREFIX=systest_builds$$
TMP_TEMPLATE="$TMP_PREFIX"_XXXXX.tmp

BUILD_LABEL=""

function usage {
cat << EOF

Required arguements:
	-k - Kernel Source directory
	-m - The map with the test suite name and the corresponding information
	-d - The defconfig to be built from the defconfig builder.
	-b - output directory for the build
	-t - TestSuite to be built.  This will also be appeneded to the output
	     build directory

Optional arguements:
	-a - Arch to build either "arm" or "arm64" this can also be exported as
             an enviroment variable
	-o - DT overlay target directory
	-w - Working directory for the test scripts.
	-l - Build label or tag to use for modules output.

Example:
	./build_system_test.sh -k ~/linux_kernel/upstream/ -b ~/Desktop/build_out
        -t j7_usb_otg_high_speed -d ti_sdk_arm64_release -a arm64 -o usb_otg/j7/high_speed
EOF
}

function prepare_for_exit() {
	D=$(dirname "$SYSTEM_TEST_LOG")
	rm -f "$D"/"$TMP_PREFIX"*.tmp
	exit
}

function set_working_directory() {
	if [ ! -d "$KERNEL_PATH" ]; then
		KERNEL_PATH=$(pwd)
	fi

	ORIGINAL_DIR=$(pwd)
	if [ "$ORIGINAL_DIR" != "$KERNEL_PATH" ]; then
		cd "$KERNEL_PATH"
		KERNEL_PATH=$(pwd)
	fi
}

function build_defconfig() {
	cd ${DEFCONFIG_DIR}
	${DEFCONFIG_DIR}/defconfig_builder.sh -k ${KERNEL_PATH} -o ${BUILD_OUTPUT} -t ${DEFCONFIG_BASE}
}

function copy_defconfig_to_kernel() {
	# Set read permissions for the defconfig
	chmod 644 ${BUILD_OUTPUT}/${DEFCONFIG}
	cp -v ${BUILD_OUTPUT}/${DEFCONFIG} ${ARCH_PATH}/.
}

function remove_defconfig_from_kernel() {
	rm ${ARCH_PATH}/${DEFCONFIG}
}

function check_for_compiler {
	COMPILER_COMMAND=$(which "$CROSS_COMPILE""gcc")
	if [ -x "$COMPILER_COMMAND" ]; then
		echo "Found " "$CROSS_COMPILE"
	else
		echo "Invalid or non-existent compiler ""$COMPILER_COMMAND" > build_failure.txt
		exit 1
	fi

	export CROSS_COMPILE=${CROSS_COMPILE}
}

function cross_make {
	make -j"$BUILD_THREADS" $*
}

function clean_up_on_fail() {
	cross_make ARCH=${ARCH} O=${KERNEL_OUT} distclean
	rm -rf ${KERNEL_OUT}
}

function build_the_kernel() {
	cross_make ARCH=${ARCH} O=${KERNEL_OUT}
	if [ "$?" -gt 0 ]; then
		echo "Building the ${TESTSUITE} kernel failed"
		clean_up_on_fail
		exit 1
	fi

	cross_make ARCH=${ARCH} O=${KERNEL_OUT} dtbs
	if [ "$?" -gt 0 ]; then
		echo "Building the ${TESTSUITE} dtbs failed"
		clean_up_on_fail
		exit 1
	fi

	cross_make ARCH=${ARCH} O=${KERNEL_OUT} modules
	if [ "$?" -gt 0 ]; then
		echo "Building the ${TESTSUITE} modules failed"
		clean_up_on_fail
		exit 1
	fi

	cross_make ARCH=${ARCH} O=${KERNEL_OUT} tar-pkg
	if [ "$?" -gt 0 ]; then
		echo "Tar-pk for ${TESTSUITE} failed"
		clean_up_on_fail
		exit 1
	fi

}

function clean_the_kernel() {
	cross_make ARCH=${ARCH} O=${KERNEL_OUT} distclean
}

function build_the_overlays() {
	cross_make LINUX=${KERNEL_PATH} DTC=${KERNEL_OUT}/scripts/dtc/dtc ARCH=${ARCH} O=${KERNEL_OUT} -C ${SYS_TEST_WORKING_DIR}
}

function copy_needed_files() {
        mkdir -p ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/modules"
        mkdir -p ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree"

        TARBALL=$(ls ${KERNEL_OUT}/*.tar)
	if [ ${ARCH} = "arm64" ]; then
		if [ -f ${KERNEL_OUT}/arch/arm64/boot/Image ]; then
		     cp -v ${KERNEL_OUT}/arch/arm64/boot/Image ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/Image"
		fi

		if [ -f ${KERNEL_OUT}/arch/arm64/boot/vmlinux ]; then
		     cp -v ${KERNEL_OUT}/arch/arm64/boot/vmlinux ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/vmlinux"
		fi

	        cp -v ${KERNEL_OUT}/${ARM64_DT_PATH}/*.dtb ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
		cp -v ${KERNEL_OUT}/${ARM64_DT_PATH}/*.dtbo ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
	        cp -v $TARBALL ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/modules/"${TESTSUITE}_64bit_modules.tar
	else
	        if [ -f ${KERNEL_OUT}/arch/arm/boot/zImage ]; then
	             cp -v ${KERNEL_OUT}/arch/arm/boot/zImage ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/zImage"
	        fi

		if [ -f ${KERNEL_OUT}/arch/arm/boot/vmlinux ]; then
		     cp -v ${KERNEL_OUT}/arch/arm/boot/vmlinux ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/vmlinux"
		fi

	        cp -v ${KERNEL_OUT}/${ARM_DT_PATH}/*.dtb ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
	        cp -v ${KERNEL_OUT}/${ARM_DT_PATH}/*.dtbo ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
		if [ ${BUILD_LABEL} = "" ]; then
			cp -v $TARBALL ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/modules/"${TESTSUITE}_modules.tar
		else
			cp -v $TARBALL ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/modules/"${BUILD_LABEL}_modules.tar
		fi
	fi
        cp -v ${KERNEL_OUT}/.config ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/built_defconfig.txt"
}

function copy_overlay_files() {
	# Need to re-copy the base DTB files as they are built now with symbols
	if [ ${ARCH} = "arm64" ]; then
	        cp -v ${KERNEL_OUT}/${ARM64_DT_PATH}/*.dtb ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
		cp -v ${KERNEL_OUT}/${ARM64_DT_PATH}/*.dtbo ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
	else
	        cp -v ${KERNEL_OUT}/${ARM_DT_PATH}/*.dtb ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
	        cp -v ${KERNEL_OUT}/${ARM_DT_PATH}/*.dtbo ${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/device_tree/."
	fi
	cp -v ${OVERLAY_DIR}/*.dtbo ${BASE_OUTPUT_DIR}"/"${TESTSUITE}/device_tree/.
}

while getopts "a:k:t:w:b:d:o:l:" OPTION
do
	case $OPTION in
	a)
		ARCH=$OPTARG;;
	k)
		KERNEL_PATH=$OPTARG;;
	w)
		SYS_TEST_WORKING_DIR=$OPTARG;;
	b)
		BASE_OUTPUT_DIR=$OPTARG;;
	t)
		TESTSUITE=$OPTARG;;
	d)
		DEFCONFIG_BASE=$OPTARG;;
	o)
		OVERLAY_BASE_DIR=$OPTARG;;
	l)
		BUILD_LABEL=$OPTARG;;
	?)
		usage
		exit;;
     esac
done

trap prepare_for_exit SIGHUP EXIT SIGINT SIGTERM

BUILD_THREADS=16

if [ -z ${SYS_TEST_WORKING_DIR} ]; then
	SYS_TEST_WORKING_DIR=$(pwd)
fi
DEFCONFIG_DIR=${SYS_TEST_WORKING_DIR}"/config"

if [ -z ${KERNEL_PATH} ]; then
	echo "Need a path to the Linux Kernel"
	exit 1
fi
if [ -z ${CROSS_COMPILE} ]; then
	echo ${CROSS_COMPILE}
fi

if [ -z ${ARCH} ]; then
	echo "Please set and export ARCH to arm or arm64"
	exit 1
fi

if [ -z ${TESTSUITE} ]; then
	echo "Testsuite ${TESTSUITE} must be populated" 
	exit 1
fi

if [ -z ${DEFCONFIG_BASE} ]; then
	echo "Defconfig Base must be populated"
	exit 1
fi

KERNEL_OUT=${BASE_OUTPUT_DIR}"/"${TESTSUITE}"/kernel_out"

if [ ${ARCH} = "arm64" ]; then
	ARCH_PATH="${KERNEL_PATH}/arch/arm64/configs"
	OVERLAY_DIR=${KERNEL_OUT}"/arch/arm64/boot/dts/ti/system_test/${OVERLAY_BASE_DIR}"
else
	ARCH_PATH="${KERNEL_PATH}/arch/arm/configs"
	OVERLAY_DIR=${KERNEL_OUT}"/arch/arm/boot/dts/ti/system_test/${OVERLAY_BASE_DIR}"
fi

BUILD_OUTPUT=${BASE_OUTPUT_DIR}"/"${TESTSUITE}
if [ ! -d ${BUILD_OUTPUT} ]; then
	mkdir -p ${BUILD_OUTPUT}
else
	rm -rf ${BUILD_OUTPUT}
	mkdir -p ${BUILD_OUTPUT}
fi

check_for_compiler

IN_TREE_DEFCONFIG=$(grep -c "defconfig" <<< ${DEFCONFIG_BASE})
if [ ${IN_TREE_DEFCONFIG} -eq 0 ]; then
	DEFCONFIG=${DEFCONFIG_BASE}"_defconfig"
	build_defconfig
	copy_defconfig_to_kernel
else
	DEFCONFIG=${DEFCONFIG_BASE}
fi

set_working_directory

cross_make ARCH=${ARCH} O=${KERNEL_OUT} ${DEFCONFIG}

if [ ${IN_TREE_DEFCONFIG} -eq 0 ]; then
	remove_defconfig_from_kernel
fi

build_the_kernel

copy_needed_files

if [ ! -z ${OVERLAY_DIR} ]; then
	build_the_overlays
	copy_overlay_files
fi

cross_make ARCH=${ARCH} O=${KERNEL_OUT} distclean

rm -rf ${KERNEL_OUT}

exit 0
